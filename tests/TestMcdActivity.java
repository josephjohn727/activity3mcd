import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestMcdActivity {
	
	
	//Global driver variable	
	WebDriver driver;	
	
	// location of the chromedriver	
	final String ChromeDriverLocation = "/Users/owner/Desktop/chromedriver";
	
	// website to be tested	
	final String UrlToBeTested = "https://www.mcdonalds.com/ca/en-ca.html" ;

	@Before
	public void setUp() throws Exception {
		//selenium setup
		System.setProperty("webdriver.chrome.driver", ChromeDriverLocation);
		driver = new ChromeDriver();
		
		// go to the website
		driver.get(UrlToBeTested);
	}

	@After
	public void tearDown() throws Exception {
		// After you run the test case, close the browser
			Thread.sleep(5000);
			driver.close();
	}

	@Test
	public void test() {
		

		//Calling func for Test case 1
		TestTC1();
		
		//Calling func for Test case 2
		TestTC2();
		

		//Calling func for Test case 3
		TestTC3();		
		
	
		
		
	}
	
	//Func for Test Case 1
	void TestTC1(){
		//TC1:  Title of the subscription feature is “Subscribe to my Mcd’s”
				WebElement elementTitleSubscribeMcd = driver.findElement(By.cssSelector("h2.click-before-outline"));
				assertEquals( "Subscribe to My McD’s®", elementTitleSubscribeMcd.getText());
				
	}
	
	//Func for Test Case 2
	void TestTC2(){
		//TC2:  Email signup - happy path
		
				//Enter the First Name
				WebElement firstNameTextField = driver.findElement(By.id("firstname2"));
				firstNameTextField.sendKeys("Joseph John");
				
				//Enter the E-mail
				WebElement emailTextField = driver.findElement(By.id("email2"));
				emailTextField.sendKeys("josephjohn727@gmail.com");
				
				//Enter first 3 digits of a valid postal code (letter-number-letter)
				WebElement postalcodeFirst3 = driver.findElement(By.id("postalcode2"));
				
				postalcodeFirst3.sendKeys("M1E");
				
				//User presses subscribe button
				WebElement subscribeButtonElement = driver.findElement(By.id("g-recaptcha-btn-2"));
				subscribeButtonElement.click();
				
				
		
	}
	
	//Func for Test Case 3
	void TestTC3() {

		//TC3: Email signup -  negative case
		
		//User presses subscribe button
		WebElement subscribeButtonElement = driver.findElement(By.id("g-recaptcha-btn-2"));
				subscribeButtonElement.click();
		
		
	}

}
